<?php

use Phinx\Migration\AbstractMigration;

class CreateJosUsersTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $userProfileTypes = $this->table('jobs_users');
        $userProfileTypes->addColumn('user_id', 'integer')
                         ->addColumn('job_id', 'integer')
                         ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE jobs_users");
    }
}

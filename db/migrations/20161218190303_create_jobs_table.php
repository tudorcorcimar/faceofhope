<?php

use Phinx\Migration\AbstractMigration;

class CreateJobsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $donations = $this->table('jobs');
        $donations->addColumn('title', 'text', array('null' => true))
                    ->addColumn('details', 'text', array('null' => true))
                    ->create();
    }
}

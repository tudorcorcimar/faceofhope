<?php

use Phinx\Migration\AbstractMigration;

class CreateContactsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('contacts');
        $user->addColumn('photo', 'text', array('null' => true))
             ->addColumn('details', 'text', array('null' => true))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE contacts");
    }
}

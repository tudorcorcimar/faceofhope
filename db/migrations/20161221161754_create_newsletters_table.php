<?php

use Phinx\Migration\AbstractMigration;

class CreateNewslettersTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $newsletters = $this->table('newsletters');
        $newsletters->addColumn('title', 'text', array('null' => true))
                    ->addColumn('details', 'text', array('null' => true))
                    ->addColumn('sent', 'boolean', array('null' => false, 'default' => false))
                    ->addColumn('created_at', 'datetime')
                    ->addColumn('updated_at', 'datetime')
                    ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE newsletters");
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class CreateSubscribersTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $subscribers = $this->table('subscribers');
        $subscribers->addColumn('email', 'string', array('limit' => 255, 'null' => false))
                    ->addColumn('subscribed', 'boolean', array('default' => true))
                    ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE subscribers");
    }
}

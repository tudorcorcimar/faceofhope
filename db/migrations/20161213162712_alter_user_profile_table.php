<?php

use Phinx\Migration\AbstractMigration;

class AlterUserProfileTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $profile = $this->table('user_profile');
        $profile->addColumn('address', 'text', array('null' => true))
             ->addColumn('city', 'string', array('limit' => 50, 'null' => true))
             ->addColumn('state', 'string', array('limit' => 50, 'null' => true))
             ->addColumn('zip', 'integer', array('limit' => 15, 'null' => true))
             ->addColumn('birth_day', 'datetime', array('null' => true))
             ->addColumn('other_contact', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('emergency_contact', 'string', array('limit' => 255, 'null' => true))
             ->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {

    }
}

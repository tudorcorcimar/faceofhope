<?php

use Phinx\Migration\AbstractMigration;

class CreateAdminTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up() {
        $this->execute("CREATE TABLE admin ("
                       . " id INT(11) NOT NULL AUTO_INCREMENT, "
                       . " permissions VARCHAR(255) DEFAULT '', "
                       . " name VARCHAR(255) DEFAULT '', "
                       . " surname VARCHAR(255) DEFAULT '', "
                       . " login VARCHAR(150) DEFAULT '', "
                       . " password VARCHAR(150) DEFAULT '', "
                       . " PRIMARY KEY (id) );");
        
        $this->execute("INSERT INTO admin "
            . " (permissions, name, surname, login, password) "
            . " VALUES "
            . "('admin', "
            . "'Paul', "
            . "'Johnson', "
            . "'admin@faceofhope211.org', "
            . " 'vZRtrB17U');");
    }

    /**
     * Migrate Down.
     */
    public function down() {
        $this->execute("DROP TABLE admin;");
    }
}

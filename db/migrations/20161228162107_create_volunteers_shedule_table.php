<?php

use Phinx\Migration\AbstractMigration;

class CreateVolunteersSheduleTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $shedule = $this->table('v_shedule');
        $shedule->addColumn('user_id', 'integer')
             ->addColumn('monday', 'text', array('null' => true))
             ->addColumn('tuesday', 'text', array('null' => true))
             ->addColumn('wednesday', 'text', array('null' => true))
             ->addColumn('thursday', 'text', array('null' => true))
             ->addColumn('friday', 'text', array('null' => true))
             ->addColumn('saturday', 'text', array('null' => true))
             ->addColumn('sunday', 'text', array('null' => true))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE v_shedule");
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class CreateDonorTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('donors');
        $user->addColumn('user_id', 'integer')
             ->addColumn('is_organization', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('organization_name', 'string', array('limit' => 255, 'null' => true))
             ->addColumn('active', 'boolean', array('default' => true))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE donors");
    }
}

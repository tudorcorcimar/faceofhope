<?php

use Phinx\Migration\AbstractMigration;

class CreateTableApp extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $app = $this->table('app');
        $app->addColumn('name', 'string', array('limit' => 255))
             ->addColumn('phone', 'string', array('limit' => 100))
             ->addColumn('fax', 'string', array('limit' => 100))
             ->addColumn('email', 'string', array('limit' => 100))
             ->addColumn('address', 'string', array('limit' => 100))
             ->addColumn('url', 'string', array('limit' => 100))
             ->addColumn('description', 'text')
             ->create();
        
        $rows = [
            [
              'name'  => 'Face Of Hope',
              'phone'  => '941 257 8495',
              'fax'  => '',
              'email'  => 'faceofhope211@gmail.com',
              'address'  => 'BISCAYNE PLAZA 13600 Tamiami Trail North Port, FL 34287',
              'url'  => 'faceofhope211.org',
              'description'  => '
                  We provide comprehensive social and outreach services to the poor and homeless, addressing their immediate needs, such as food, clothing, emergency shelter, affordable housing, vacational training and employment.
              ',
                
            ]
        ];

        $this->insert('app', $rows);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE app");
    }
}

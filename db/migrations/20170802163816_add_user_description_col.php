<?php

use Phinx\Migration\AbstractMigration;

class AddUserDescriptionCol extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('user');
        $user->addColumn('description', 'text', array('null' => true))
             ->update();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $table = $this->table('user');
        $table->removeColumn('description')
              ->save();
    }
}

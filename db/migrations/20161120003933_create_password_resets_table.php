<?php

use Phinx\Migration\AbstractMigration;

class CreatePasswordResetsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $password_resets = $this->table('password_resets');
        $password_resets->addColumn('email', 'string', array('limit' => 255))
                        ->addColumn('token', 'string', array('limit' => 255))
                        ->addColumn('created_at', 'datetime')
                        ->addIndex(array('token', 'email'), array('unique' => true))
                        ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE password_resets");
    }

}

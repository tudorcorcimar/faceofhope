<?php

use Phinx\Migration\AbstractMigration;

class CreateValunteerTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('volunteer');
        $user->addColumn('user_id', 'integer')
             ->addColumn('active', 'boolean', array('default' => true))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE volunteer");
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUserTypes extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $types = $this->table('user_types');
        
        $types->addColumn('name', 'string', array('limit' => 30, 'null' => false))
              ->create();
        
        $rows = [
            [
              'name'  => 'donor'
            ],
            [
              'name'  => 'volunteer'
            ],
            [
              'name'  => 'needy'
            ],
            [
              'name'  => 'veteran'
            ]
        ];

        $this->insert('user_types', $rows);
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE user_types");
    }
}

<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUserProfileTypes extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $userProfileTypes = $this->table('user_profile_types');
        $userProfileTypes->addColumn('user_id', 'integer')
                         ->addColumn('type_id', 'integer')
                         ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE user_profile_types");
    }
}

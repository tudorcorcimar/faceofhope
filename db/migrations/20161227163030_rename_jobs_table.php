<?php

use Phinx\Migration\AbstractMigration;

class RenameJobsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $this->execute('RENAME TABLE jobs TO v_jobs;');
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE v_jobs");
    }
}

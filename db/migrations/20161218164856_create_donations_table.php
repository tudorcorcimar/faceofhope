<?php

use Phinx\Migration\AbstractMigration;

class CreateDonationsTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $donations = $this->table('donations');
        $donations->addColumn('user_id', 'integer')
                    ->addColumn('money', 'integer', array('null' => true))
                    ->addColumn('stuff', 'text', array('null' => true))
                    ->addColumn('comments', 'text', array('null' => true))
                    ->addColumn('created_at', 'datetime')
                    ->addColumn('updated_at', 'datetime', array('null' => true))
                    ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE donations");
    }
}

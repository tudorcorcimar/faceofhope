<?php

use Phinx\Migration\AbstractMigration;

class CreateEventSubscribersTable extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $userProfileTypes = $this->table('event_subscribers');
        $userProfileTypes->addColumn('first_name', 'string', array('limit' => 255, 'null' => false))
                         ->addColumn('last_name', 'string', array('limit' => 255, 'null' => false))
                         ->addColumn('email', 'string', array('limit' => 255, 'null' => false))
                         ->addColumn('phone', 'string', array('limit' => 255, 'null' => false))
                         ->addColumn('address', 'text', array('null' => true))
                         ->addColumn('event_type', 'string', array('null' => true))
                         ->addColumn('event_title', 'text', array('null' => true))
                         ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE event_subscribers");
    }
}

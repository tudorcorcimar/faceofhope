<?php

use Phinx\Migration\AbstractMigration;

class CreateTableUserProfile extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $userProfile = $this->table('user_profile');
        $userProfile->addColumn('user_id', 'integer')
                    ->addColumn('photo', 'text')
                    ->addColumn('phone', 'string', array('limit' => 50, 'null' => true))
                    ->addColumn('description', 'text')
                    ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE user_profile");
    }
}

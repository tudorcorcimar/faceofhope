<?php

use Phinx\Migration\AbstractMigration;

class CreateNeedy extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        $user = $this->table('needy');
        $user->addColumn('user_id', 'integer')
             ->addColumn('details', 'text')
             ->addColumn('active', 'boolean', array('default' => true))
             ->create();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        $this->execute("DROP TABLE needy");
    }
}

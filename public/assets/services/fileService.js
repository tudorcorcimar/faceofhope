
var getImageCount = function(){
    return $('.uploaded-image').length;
};

if(getImageCount() < 3){
    $('.upload-image-button').removeClass('hidden');
}
  
/*
 * Prepare file and set max width
 */
function dropChangeHandler (e) {
    e.preventDefault();
    e = e.originalEvent;
    var target = e.dataTransfer || e.target;
    var file = target && target.files && target.files[0];

    var options = {
        maxWidth: 500,
        maxHeight: 500,
        minWidth: 450,
        minHeight: 450,
        canvas: true
    };
    
    if (!file) { return; }
    loadImage.parseMetaData(file, function (data) {
    displayImage(file, options);
  });
}

/*
 * Create image
 */
function displayImage (file, options) {
    if (!loadImage( file, replaceResults, options )){
        $('.temp-block').replaceWith(
            $('<span>Your browser does not support the URL or FileReader API.</span>')
        );
    }
}

/*
 * Add new image to content
 */
function replaceResults (img) {
    var content;
    if (!(img.src || img instanceof HTMLCanvasElement)) {
        content = $('<span>Loading image file failed</span>');
    } else {
        var imageUrl = img.src || img.toDataURL();
               
        $('.image .mini-image .image-back').css('background', 'url(' + imageUrl + ') center center no-repeat')
                              .css('background-size', 'cover');
        $('textarea.base-64-storage').append(imageUrl)
                                     .prop('disabled', false);
        $('.image-uploader-loader').hide();
    }
}

$('.upload-image-button').on('click', function(){
    $('.images-uploader-block > input[type="file"]').click();
});

$('.images-uploader-block > input[type="file"]').on('change', function (e) {
    
    if( this.files.length === 0 ){
        $('.image-uploader-loader').hide();
    }else{
        dropChangeHandler(e);
    }
    
});
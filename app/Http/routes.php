<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
 * Frontend routes
 */


Route::group(['prefix' => '/'], function () {
    
    
    /*
     * Home Routes
     */
    Route::get('/', [ 'uses' => 'Frontend\Home\HomeController@showHome']);
    Route::get('/contactus', [ 'uses' => 'Frontend\Home\HomeController@showCuntactUsForm']);
    Route::post('/contactus', [ 'uses' => 'Frontend\Home\HomeController@sendMessage']);
    
    
    /*
     * Auth Routes
     */
    Route::auth();
    
    
    /*
     * Donors Routes
     */
    Route::group(['prefix' => 'donating'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Donating\DonatingController@index']);
    });
    
    
    /*
     * Volunteering Routes
     */
    Route::group(['prefix' => 'volunteering'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Volunteering\VolunteeringController@index']);
        Route::get('/jobs/apply/{jobId}', [ 'uses' => 'Frontend\Volunteering\VolunteeringController@applyForPosition']);
        Route::get('/positions', [ 'uses' => 'Frontend\Volunteering\VolunteeringController@showPositions']);
        Route::get('/position/{id}', [ 'uses' => 'Frontend\Volunteering\VolunteeringController@showPosition']);
    });
    
    
    /*
     * Veterans Routes
     */
    Route::group(['prefix' => 'veterans'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Veterans\VeteransController@index']);
    });
    
    
    /*
     * Citizenship Routes
     */
    Route::group(['prefix' => 'citizenship'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Citizenship\CitizenshipController@index']);
    });
    
    
    /*
     * Projects Routes
     */
    Route::group(['prefix' => 'projects'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Projects\ProjectsController@index']);
    });
    
    
    /*
     * Newsletters Routes
     */
    Route::group(['prefix' => 'newsletters'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Newsletters\NewslettersController@index']);
        Route::get('/{id}', [ 'uses' => 'Frontend\Newsletters\NewslettersController@showNewsletter']);
        Route::post('/subscribe', [ 'uses' => 'Frontend\Newsletters\NewslettersController@subscribe']);
    });
    Route::get('/unsubscribe', [ 'uses' => 'Frontend\Newsletters\NewslettersController@unsubscribe']);
    
    
    /*
     * Citizenship Routes
     */
    Route::group(['prefix' => 'events'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Events\EventsController@index']);
        Route::post('/apply', [ 'uses' => 'Frontend\Events\EventsController@registerEventDonor']);
    });
    
    
    /*
     * Profile Routes
     */
    Route::group(['middleware' => 'auth', 'prefix' => 'profile'], function () {
        Route::get('/', [ 'uses' => 'Frontend\Profile\SettingsController@showSettings']);
        Route::get('/settings', [ 'uses' => 'Frontend\Profile\SettingsController@showSettings']);
        Route::post('/settings', [ 'uses' => 'Frontend\Profile\SettingsController@updateUserSettings']);
        Route::post('/settings/email', [ 'uses' => 'Frontend\Profile\SettingsController@updateUserEmail']);
        Route::post('/settings/password', [ 'uses' => 'Frontend\Profile\SettingsController@updateUserPassword']);
        Route::get('/donations', [ 'uses' => 'Frontend\Profile\DonationsController@showDonations']);
        Route::get('/volunteerings', [ 'uses' => 'Frontend\Profile\VolunteeringsController@showVolunteerings']);
        Route::post('/volunteerings/shedule/{userId}', [ 'uses' => 'Frontend\Profile\VolunteeringsController@saveShedule']);
        Route::get('/needs', [ 'uses' => 'Frontend\Profile\NeedsController@showNeeds']);
        Route::post('/needs', [ 'uses' => 'Frontend\Profile\NeedsController@updateNeeds']);
        Route::get('/uploads', [ 'uses' => 'Frontend\Profile\UploadsController@showUploads']);
        Route::post('/uploads/photo', [ 'uses' => 'Frontend\Profile\UploadsController@savePhoto']);
    });
    
    
    /*
     * Admin Routes
     */
    Route::group(['prefix' => 'admin'], function(){
        
        /*
         * Admin auth Routes
         */
        Route::get('/', [ 'uses' => 'Admin\Auth\AdminAuthController@showSignIn']);
        Route::get('/auth', [ 'uses' => 'Admin\Auth\AdminAuthController@showSignIn']);
        Route::post('/auth/signin', [ 'uses' => 'Admin\Auth\AdminAuthController@signIn']);
        Route::get('/auth/signout', [ 'uses' => 'Admin\Auth\AdminAuthController@signOut']);
        
        /*
         * Admin home Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'home'], function(){
            Route::get('/', [ 'uses' => 'Admin\Home\HomeController@index']);

        });
        
        /*
         * Admin application settings Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'app'], function(){
            Route::get('/', [ 'uses' => 'Admin\App\AppController@index']);
            Route::post('/', [ 'uses' => 'Admin\App\AppController@updateAppInfo']);
            Route::post('/updatepassword', [ 'uses' => 'Admin\App\AppController@updateAdminPassword']);
        });
        
        /*
         * Admin users Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'users'], function(){
            Route::get('/', [ 'uses' => 'Admin\Users\UsersController@index']);
            Route::post('/search', [ 'uses' => 'Admin\Users\UsersController@search']);
            Route::get('/status/change/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@switchUserActiveStatus']);
            Route::get('/remove/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@removeUser']);
            Route::get('/edit/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@index']);
            
            Route::post('/edit/settings/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@updateUserSettings']);
            Route::post('/edit/email/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@updateUserEmail']);
            Route::post('/edit/password/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@updateUserPassword']);
            Route::post('/edit/needy/{userId}', [ 'uses' => 'Admin\Users\EditUsersController@updateNeeds']);

            Route::get('/create', [ 'uses' => 'Admin\Users\CreateUsersController@index']);
            Route::post('/create', [ 'uses' => 'Admin\Users\CreateUsersController@createUser']);
        });
        
        /*
         * Admin donors Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'donors'], function(){
            Route::get('/', [ 'uses' => 'Admin\Donors\DonorsController@index']);
            Route::get('/create/donation/{userId}', [ 'uses' => 'Admin\Donors\DonorsController@showCreatingDonationForm']);
            Route::post('/create/donation', [ 'uses' => 'Admin\Donors\DonorsController@createDonation']);
            Route::get('/remove/donation/{donationId}', [ 'uses' => 'Admin\Donors\DonorsController@removeDonation']);
        });
        
        /*
         * Admin events Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'events'], function(){
            Route::get('/', [ 'uses' => 'Admin\Events\EventsController@index']);

        });
        
        /*
         * Admin jobs Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'jobs'], function(){
            Route::get('/', [ 'uses' => 'Admin\Jobs\JobsController@index']);
            Route::get('/add', [ 'uses' => 'Admin\Jobs\JobsController@showAddJobForm']);
            Route::post('/create', [ 'uses' => 'Admin\Jobs\JobsController@createPosition']);
            Route::get('/edit/{id}', [ 'uses' => 'Admin\Jobs\JobsController@showEditPosition']);
            Route::post('/edit/{id}', [ 'uses' => 'Admin\Jobs\JobsController@editPosition']);
            Route::get('/remove/{jobId}', [ 'uses' => 'Admin\Jobs\JobsController@removePosition']);
            Route::get('/applies', [ 'uses' => 'Admin\Jobs\JobsController@showAppliesUsers']);
        });
        
        /*
         * Admin newsletters Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'newsletters'], function(){
            Route::get('/', [ 'uses' => 'Admin\Newsletters\NewslettersController@index']);
            Route::get('/create', [ 'uses' => 'Admin\Newsletters\NewslettersController@showNewsletterCreate']);
            Route::post('/create', [ 'uses' => 'Admin\Newsletters\NewslettersController@createNewsletter']);
            Route::get('/edit/{id}', [ 'uses' => 'Admin\Newsletters\NewslettersController@showEditNewsletter']);
            Route::post('/edit/{id}', [ 'uses' => 'Admin\Newsletters\NewslettersController@editNewsletter']);
            Route::get('/remove/{newsId}', [ 'uses' => 'Admin\Newsletters\NewslettersController@removeNewsletter']);
            Route::get('/subscribers', [ 'uses' => 'Admin\Newsletters\NewslettersController@showSubscribers']);
            Route::get('/send/{id}', [ 'uses' => 'Admin\Newsletters\NewslettersController@sendNewsletter']);
            Route::get('/subscribe/{id}', [ 'uses' => 'Admin\Newsletters\NewslettersController@switchSubscribe']);
        });
        
        /*
         * Admin contacts Routes
         */
        Route::group(['middleware' => 'adminAuth', 'prefix' => 'contacts'], function(){
            Route::get('/', [ 'uses' => 'Admin\Contacts\ContactsController@showAdministration']);
            Route::post('/administration/create', [ 'uses' => 'Admin\Contacts\ContactsController@addAdministrationInfo']);
            Route::get('/edit/{id}', [ 'uses' => 'Admin\Contacts\ContactsController@showEditAdministrationInfo']);
            Route::post('/edit/{id}', [ 'uses' => 'Admin\Contacts\ContactsController@editAdministrationInfo']);
            Route::get('/remove/{id}', [ 'uses' => 'Admin\Contacts\ContactsController@removeAdministrationInfo']);
        });
        
    });
    
    
    /*
     * Locations Routes
     */
    Route::group(['prefix' => 'locations'], function(){
        Route::get('/cities/{stateAbbr}', [ 'uses' => 'Locations\LocationsController@getCitiesByState']);
    });
    
});


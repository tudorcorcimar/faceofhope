<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Runtime\AdminAuth as AdminAuthentication;
use Illuminate\Support\Facades\Redirect;

class AdminAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isAuth = AdminAuthentication::getInstance()->getIsAuth();
        
        if( !$isAuth ){
            return Redirect::to('admin/auth');
        }
        
        return $next($request);
    }
}

<?php

namespace App\Http\Controllers\Locations;

use App\Http\Controllers\Controller;
use App\Services\ViewBagService;

class LocationsController extends Controller
{
    public function getCitiesByState($stateAbbr) {
        $viewBagService = new ViewBagService();
        $cities = $viewBagService->getCitiesByState($stateAbbr)->toArray();
        
        return response()->json($cities);
    }
}

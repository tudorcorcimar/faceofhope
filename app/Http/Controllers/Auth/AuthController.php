<?php

namespace App\Http\Controllers\Auth;

use Validator;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Http\Controllers\Frontend\FrontEndBaseController;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileTypes;
use App\Models\Needy;
use App\Models\Volunteer;
use App\Models\Donor;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;

class AuthController extends FrontEndBaseController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile/settings';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }
    
    /**
     * Rerwrite showRegistrationForm methot from RegisterUser trait
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        if (property_exists($this, 'registerView')) {
            return view($this->registerView);
        }
        
        return view('auth.register', $this->viewBag);
    }

    /**
     * Show the application login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $view = property_exists($this, 'loginView')
                    ? $this->loginView : 'auth.authenticate';

        if (view()->exists($view)) {
            return view($view);
        }

        return view('auth.login', $this->viewBag);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|max:255',
            'middle_name' => 'max:150',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user',
            'phone' => 'required|max:50',
            'types' => 'required|not_in:0',
            'password' => 'required|min:6|confirmed',
            'address' => 'required',
            'zip' => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $isNeedy = false;
        $isDonor = false;
        $isVolunteer = false;
        
        $userTypeName = "";
        
        $user = new User;
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->middle_name = $data['middle_name'];
        $user->email = $data['email'];
        $user->active = true;
        $user->password = bcrypt($data['password']);
        $user->save();
        
        $userProfile = new UserProfile;
        $userProfile->user_id = $user->id;
        $userProfile->phone = $data['phone'];
        $userProfile->address = $data['address'];
        $userProfile->city = $data['city'];
        $userProfile->state = $data['state'];
        $userProfile->zip = $data['zip'];
        if(isset($data['birth_day']) && $data['birth_day'] !== ''  ){
            $userProfile->birth_day = Carbon::createFromFormat('F.d.Y', $data['birth_day']);
        }
        $userProfile->emergency_contact = $data['emergency_contact'];
        $userProfile->save();
        
        if(isset($data['types']) && is_array($data['types']) ){
            foreach($data['types'] as $key => $typeId){
                if( $key === 3 || $key === 4 ){
                    $isNeedy = true;
                    $userTypeName = "needy";
                }
                if( $key === 2 ){
                    $isVolunteer = true;
                    $userTypeName = "volunteer";
                }
                if( $key === 1 ){
                    $isDonor = true;
                    $userTypeName = "donor";
                }
                $userProfileTypes = new UserProfileTypes;
                $userProfileTypes->user_id = $user->id;
                $userProfileTypes->type_id = $key;
                $userProfileTypes->save();
            }
        }
        
        if($isNeedy){
            $details = json_encode($data['needy']);
            $needy = new Needy;
            $needy->user_id = $user->id;
            $needy->details = $details;
            $needy->save();
        }
        
        if($isVolunteer){
            $volunteer = new Volunteer;
            $volunteer->user_id = $user->id;
            $volunteer->active = true;
            $volunteer->save();
        }
        
        if($isDonor){
            $donor = new Donor;
            $donor->user_id = $user->id;
            $donor->active = true;
            $donor->save();
        }
        
        $this->sendRegistrationNotice($data, $userTypeName);
        
        return $user;
    }
    
    public function sendRegistrationNotice($data, $userTypeName) {
        
        $appMail = env('MAIL_USERNAME');
        Mail::send('emails.registernotification', $data, function ($message) use ($appMail, $data, $userTypeName) {
            $message->from($appMail, $data['first_name']);
            $message->to($appMail)
                    ->subject("New $userTypeName is registered: " . $data['first_name'] . " " . $data['last_name']);
        });
        
    }
}

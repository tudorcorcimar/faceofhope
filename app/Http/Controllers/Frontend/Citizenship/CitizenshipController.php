<?php

namespace App\Http\Controllers\Frontend\Citizenship;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;


class CitizenshipController extends FrontEndBaseController
{
    
    public function index()
    {
        return View::make('frontend.citizenship.view', $this->viewBag);
    }
    
}

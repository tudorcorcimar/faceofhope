<?php

namespace App\Http\Controllers\Frontend\Projects;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;


class ProjectsController extends FrontEndBaseController
{
    
    public function index()
    {
        return View::make('frontend.projects.view', $this->viewBag);
    }
    
}

<?php

namespace App\Http\Controllers\Frontend\Volunteering;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use App\Models\Jobs;
use App\Models\JobsApply;

class VolunteeringController extends FrontEndBaseController
{
    
    public function index()
    {
        $this->viewBag['jobs'] = Jobs::orderBy('id', 'desc')->paginate(10);
        
        return View::make('frontend.volunteering.view', $this->viewBag);
    }
    
    public function applyForPosition($jobId)
    {
        $jobs = new JobsApply;
        $jobs->user_id = $this->viewBag['view']->user->id;
        $jobs->job_id = $jobId;
        $jobs->save();
        
        return redirect('/volunteering/positions');
    }
    
    public function showPositions()
    {
        $this->viewBag['jobs'] = Jobs::orderBy('id', 'desc')->paginate(10);
        
        return View::make('frontend.volunteering.positions.search', $this->viewBag);
    }
    
    public function showPosition($id)
    {
        $this->viewBag['job'] = Jobs::find($id);
        
        return View::make('frontend.volunteering.positions.view', $this->viewBag);
    }
    
}

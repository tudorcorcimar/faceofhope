<?php

namespace App\Http\Controllers\Frontend\Home;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\Contacts;


class HomeController extends FrontEndBaseController
{

    /**
     * Show the application home page.
     *
     * @return void
     */
    public function showHome()
    {
        return View::make('frontend.home.home', $this->viewBag);
    }
    
    public function showCuntactUsForm() {
        $this->viewBag['contacts'] = Contacts::all();
        return View::make('frontend.home.contactus', $this->viewBag);
    }
    
    public function sendMessage() {
        $inputs = Input::all();
        $validator = Validator::make($inputs, [
            'name' => 'required|max:255',
            'email' => 'email|required|max:150',
            'subject' => 'required'
        ]);
        if($validator->fails()){
            return redirect("contactus")->withErrors($validator)->withInput();
        }
        $appMail = env('MAIL_USERNAME');
        Mail::send('emails.contactus', $inputs, function ($message) use ($appMail, $inputs) {
            $message->from($appMail, $inputs['name']);
            $message->to($appMail)
                    ->subject($inputs['name']);
        });
        
        
        
        return redirect('contactus')->with('success-message', 'Your message successfully sent!');
    }
    
}

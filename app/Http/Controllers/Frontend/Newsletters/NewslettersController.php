<?php

namespace App\Http\Controllers\Frontend\Newsletters;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use App\Models\Newsletters;
use Illuminate\Support\Facades\Input;
use App\Models\Subscribers;


class NewslettersController extends FrontEndBaseController
{
    
    public function index()
    {
        $this->viewBag['newsletters'] = Newsletters::orderBy('created_at', 'desc')->paginate(10);
        return View::make('frontend.newsletters.search', $this->viewBag);
    }
    
    public function showNewsletter($id)
    {
        $this->viewBag['newsletter'] = Newsletters::find($id);
        return View::make('frontend.newsletters.view', $this->viewBag);
    }
    
    public function subscribe() {
        $email = Input::get('subscribe_email');
        $subscriber = Subscribers::where('email', '=', $email )->first();
        if(!$subscriber && $email){
            $newSubscriber = new Subscribers;
            $newSubscriber->email = $email;
            $newSubscriber->save();
        }
        return redirect('newsletters')->with('popup-message', 'You are successfuly subscribed!');
    }
    
    public function unsubscribe() {
        $hash = Input::get('unsubscribe');
        $email = decrypt($hash);
        
        $subscriber = Subscribers::where('email', '=', $email )->first();
        if($subscriber){
            $subscriber->subscribed = false;
            $subscriber->save();
        }
        return redirect('newsletters')->with('popup-message', 'You are successfuly unsubscribed!');
    }
    
    
    
}

<?php

namespace App\Http\Controllers\Frontend\Donating;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use App\Models\Donations;


class DonatingController extends FrontEndBaseController
{
    
    public function index()
    {
        $this->viewBag['donations'] = Donations::paginate(10);
        
        return View::make('frontend.donating.view', $this->viewBag);
    }
    
}

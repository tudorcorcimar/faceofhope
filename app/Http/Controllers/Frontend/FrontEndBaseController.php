<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\ViewBagService;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Route;

class FrontEndBaseController extends Controller
{
    /*
     * FrontEnd view data array
     * 
     * @var Array
     */
    public $viewBag;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setViewBag();
    }
    
    /**
     * Set common view data to base $viewBag collector
     *
     * @return void
     */
    private function setViewBag()
    {
        $viewBag = new ViewBagService;
        
        $viewBag->setIsAuth(Auth::check());
        $viewBag->setUser(Auth::user());
        $viewBag->setForm(Input::all());
        $viewBag->setBreadcrumbs(Route::current());
        
        $this->viewBag['view'] = $viewBag;
    }
    
}

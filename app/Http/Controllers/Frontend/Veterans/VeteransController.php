<?php

namespace App\Http\Controllers\Frontend\Veterans;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;


class VeteransController extends FrontEndBaseController
{
    
    public function index()
    {
        return View::make('frontend.veterans.view', $this->viewBag);
    }
    
}

<?php

namespace App\Http\Controllers\Frontend\Events;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\EventSubscribers;
use Illuminate\Support\Facades\File;


class EventsController extends FrontEndBaseController
{
    public function index()
    {
        return View::make('frontend.events.view', $this->viewBag);
    }
    
    public function registerEventDonor() {
        
        $input = Input::all();
        $subscriberAddress = $input['address'].' '.$input['state'].' '.$input['city'].', '.$input['zip'];
        
        $validator = Validator::make(Input::all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|max:50'
        ]);
        
        if($validator->fails()){
            return redirect('/events#applyFormEvent')->with('errors', $validator->errors())->withInput();
        }
        
        $eventSubscriber = new EventSubscribers();
        
        $eventSubscriber->event_title = "FOUNDRAISER EVENT";
        if(isset($input['event_type'])){
            $eventSubscriber->event_type = $input['event_type'];
        }else{
            $eventSubscriber->event_type = 'gold';
        }
        
        if(isset($input['event_food'])){
            $eventSubscriber->event_type = $eventSubscriber->event_type . ' | ' . $input['event_food'];
        }
        
        $eventSubscriber->address = $subscriberAddress ? $subscriberAddress : '';
        
        $eventSubscriber->first_name = $input['first_name'];
        $eventSubscriber->last_name = $input['last_name'];
        $eventSubscriber->email = $input['email'];
        $eventSubscriber->phone = $input['phone'];
        
        $eventSubscriber->save();
        
        return redirect('/events')->with('paymentsuccessed', $input['event_type']);
        
    }
    
    
}

<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Services\ImageService;


class UploadsController extends FrontEndBaseController
{

    /**
     * Show the user profile uploads view.
     *
     * @return void
     */
    public function showUploads()
    {
        $imageService = new ImageService;
        $this->viewBag['photo'] = $imageService->getPhoto($this->viewBag['view']->user->id);
        
        return View::make('frontend.profile.uploads', $this->viewBag);
    }

    /**
     * Save user photo.
     *
     * @return redirect
     */
    public function savePhoto() {
        $this->viewBag['form'] = Input::all();
        $rules = [
            'photo' => 'required'
        ];
        $validator = Validator::make(Input::all(), $rules);
        
        if($validator->fails()){
            return redirect('profile/uploads')->withErrors($validator);
        }
        $imageService = new ImageService;
        $imageService->replacePhoto(Input::get('photo'), $this->viewBag['view']->user->id);
        return redirect('/profile/uploads')->with('success-message', 'Photo was succesuful saved!');
    }
    
}

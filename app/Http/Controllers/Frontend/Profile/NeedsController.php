<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Needy;
use App\Models\UserProfileTypes;


class NeedsController extends FrontEndBaseController
{
    /**
     * Show the user profile needs view.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function showNeeds()
    {
        $this->viewBag['needy'] = $this->getNeedy($this->viewBag['view']->user->id);
        return View::make('frontend.profile.needs', $this->viewBag);
    }


    /**
     * Try to find needy profile and if it exists return it or return empty array.
     * 
     * @param $userId int
     * 
     * @return $data array
     */
    private function getNeedy($userId) {
        $data = [];
        $needy = Needy::where('user_id', '=', $userId)->first();
        if($needy){
            $data = [
                'id' => $needy->id,
                'active' => $needy->active,
                'details' => json_decode($needy->details, true)
            ];
        }
        return $data;
    }


    /**
     * Opdate needy data.
     * 
     * @param $userId int
     * 
     * @return Illuminate\Support\Facades\View
     */
    public function updateNeeds() {
        $needy = Needy::where('user_id', '=', $this->viewBag['view']->user->id)->first();
        if(!$needy){
            $userProfileTypes = new UserProfileTypes;
            $userProfileTypes->user_id = $this->viewBag['view']->user->id;
            $userProfileTypes->type_id = 3;
            $userProfileTypes->save();
            
            $needy = new Needy;
            $needy->user_id = $this->viewBag['view']->user->id;
            $needy->details = json_encode(Input::get('needy'));
            $needy->active = true;
            $needy->save();
        }else{
            $needy->details = json_encode(Input::get('needy'));
            $needy->update();
        }
        
        $this->viewBag['needy'] = $this->getNeedy($this->viewBag['view']->user->id);
        return View::make('frontend.profile.needs', $this->viewBag);
    }
    
}

<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use App\Models\JobsApply;
use Illuminate\Support\Facades\Input;
use App\Models\VolunteerShedule;


class VolunteeringsController extends FrontEndBaseController
{

    /**
     * Show the user profile volunteerings.
     *
     * @return void
     */
    public function showVolunteerings()
    {
        $this->viewBag['shedule'] =  $this->getShedule($this->viewBag['view']->user->id);
        $this->viewBag['jobs'] = JobsApply::where('user_id', '=', $this->viewBag['view']->user->id)
                                ->join('v_jobs', 'jobs_users.job_id', '=', 'v_jobs.id')
                                ->orderBy('job_id', 'desc')->paginate(100);
        
        return View::make('frontend.profile.volunteerings', $this->viewBag);
    }
    
    public function saveShedule($userId) {
        $shedule = VolunteerShedule::where('user_id', '=', $userId)->first();
        if(!$shedule){
            $shedule = new VolunteerShedule();
            $shedule->user_id = $userId;
        }
        $shedule->monday = json_encode(Input::get('monday'));
        $shedule->tuesday = json_encode(Input::get('tuesday'));
        $shedule->wednesday = json_encode(Input::get('wednesday'));
        $shedule->thursday = json_encode(Input::get('thursday'));
        $shedule->friday = json_encode(Input::get('friday'));
        $shedule->saturday = json_encode(Input::get('saturday'));
        $shedule->sunday = json_encode(Input::get('sunday'));
        $shedule->save();
        
        return redirect('profile/volunteerings');
        
    }
    
    private function getShedule($userId) {
        $shedule = VolunteerShedule::where('user_id', '=', $userId)->first();
        $data = [];
        if($shedule){
            $data['monday']    = json_decode($shedule->monday, true);
            $data['tuesday']   = json_decode($shedule->tuesday, true);
            $data['wednesday'] = json_decode($shedule->wednesday, true);
            $data['thursday']  = json_decode($shedule->thursday, true);
            $data['friday']    = json_decode($shedule->friday, true);
            $data['saturday']  = json_decode($shedule->saturday, true);
            $data['sunday']    = json_decode($shedule->sunday, true);
        }
        return $data;
    }
}

<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use App\Models\UserProfile;
use Carbon\Carbon;


class SettingsController extends FrontEndBaseController
{

    /**
     * Show the user profile settings.
     *
     * @return void
     */
    public function showSettings()
    {
        return View::make('frontend.profile.settings', $this->viewBag);
    }
    

    /**
     * Update the user profile settings.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserSettings() 
    {
        $validator = $this->validateUserSettings();
        if($validator->fails()){
            return redirect('profile/settings')->withErrors($validator)->withInput();
        }
        $this->updateUser($this->viewBag['view']->user->id);
        
        return redirect('profile/settings')->with('success-message', 'Settings successfully updated!');
    }
    

    /**
     * Validate settings input data.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function validateUserSettings() {
        return Validator::make(Input::all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|max:50',
            'address' => 'required',
            'birth_day' => 'required',
            'zip' => 'required',
        ]);
    }


    /**
     * Update user.
     *
     * @return App\Models\User
     */
    private function updateUser($userId) 
    {
        $input = Input::all();
        $user = User::find($userId);
          
        $user->first_name = $input['first_name'];
        $user->middle_name = $input['middle_name'];
        $user->last_name = $input['last_name'];
        $user->update();
        
        $this->updateUserProfile($user->profile->id);
        
        return $user;
    }


    /**
     * Update user profile.
     *
     * @return App\Models\UserProfile
     */
    private function updateUserProfile($profileId) 
    {
        $input = Input::all();
        $userProfile = UserProfile::find($profileId);
        $userProfile->phone = $input['phone'];
        $userProfile->address = $input['address'];
        $userProfile->city = $input['city'];
        $userProfile->state = $input['state'];
        $userProfile->zip = $input['zip'];
        $userProfile->emergency_contact = $input['emergency_contact'];
        $userProfile->birth_day = Carbon::createFromFormat('F.d.Y', $input['birth_day']);
        $userProfile->update();
        
        return $userProfile;
    }


    /**
     * Update user email.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserEmail() 
    {
        $validator = Validator::make(Input::all(), [
            'email' => 'required|email|max:255|unique:user',
        ]);
        if($validator->fails()){
            return redirect('profile/settings')->with('emailError', $validator->errors()->first());
        }
        $user = User::find($this->viewBag['view']->user->id);
        $user->email = Input::get('email');
        $user->update();
        
        return redirect('profile/settings')->with('success-message', 'Email successfully updated!');
    }


    /**
     * Update user password.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserPassword() 
    {
        $validator = Validator::make(Input::all(), [
            'password' => 'required|min:6|confirmed',
        ]);
        if($validator->fails()){
            return redirect('profile/settings')->with('passwordError', $validator->errors()->first());
        }
        $user = User::find($this->viewBag['view']->user->id);
        $user->password = bcrypt(Input::get('password'));
        $user->update();
        
        return redirect('profile/settings')->with('success-message', 'Password successfully updated!');
    }
    
}

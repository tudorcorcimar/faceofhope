<?php

namespace App\Http\Controllers\Frontend\Profile;

use App\Http\Controllers\Frontend\FrontEndBaseController;
use Illuminate\Support\Facades\View;
use App\Models\Donations;


class DonationsController extends FrontEndBaseController
{

    /**
     * Show the user profile donations view.
     *
     * @return void
     */
    public function showDonations()
    {
        $this->viewBag['donations'] = Donations::where('user_id', '=', $this->viewBag['view']->user->id)->paginate(10000);
        
        return View::make('frontend.profile.donations', $this->viewBag);
    }
    
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Runtime\AdminAuth as Auth;
use App\Services\AdminViewBagService;

class AdminBaseController extends Controller
{
    /*
     * FrontEnd view data array
     * 
     * @var Array
     */
    public $viewBag;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->setViewBag();
    }
    
    /**
     * Set common view data to base $viewBag collector
     *
     * @return void
     */
    private function setViewBag()
    {
        $viewBag = new AdminViewBagService;
        $this->viewBag['view'] = $viewBag;
        $this->viewBag['user'] = Auth::getInstance();
    }
    
}

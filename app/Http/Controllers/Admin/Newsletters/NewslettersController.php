<?php

namespace App\Http\Controllers\Admin\Newsletters;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use App\Models\Newsletters;
use App\Models\Subscribers;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class NewslettersController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'newsletters';
        $this->viewBag['newsletters'] = Newsletters::orderBy('created_at', 'desc')->paginate(50);
        return View::make("admin.newsletters.view", $this->viewBag);
    }
    
    public function showNewsletterCreate() {
        $this->viewBag['category'] = 'createnewsletters';
        
        return View::make("admin.newsletters.create", $this->viewBag);
    }
    public function createNewsletter() {
        $newsletter = new Newsletters;
        $newsletter->title = Input::get('title');
        $newsletter->details = Input::get('details');
        $newsletter->save();
        
        return redirect('/admin/newsletters');
    }
    
    public function showEditNewsletter($id) {
        $this->viewBag['newsletter'] = Newsletters::find($id);
        $this->viewBag['category'] = 'editnewsletter';
        return View::make("admin.newsletters.edit", $this->viewBag);
    }
    
    public function editNewsletter($id) {
        $newsletter = Newsletters::find($id);
        $newsletter->title = Input::get('title');
        $newsletter->details = Input::get('details');
        $newsletter->update();
        
        return redirect('/admin/newsletters');
    }

    public function removeNewsletter($newsId) {
        $newsletter = Newsletters::find($newsId);
        $newsletter->delete();
        return redirect('/admin/newsletters');
    }
    
    public function showSubscribers() {
        $this->viewBag['category'] = 'subscribers';
        $this->viewBag['subscribers'] = Subscribers::paginate(55550);
        $this->viewBag['subscriberscount'] = Subscribers::all()->count();
        return View::make("admin.newsletters.subscribers", $this->viewBag);
    }
    
    public function switchSubscribe($id) {
        $subscriber = Subscribers::find($id);
        if($subscriber){
            $subscriber->subscribed = !$subscriber->subscribed;
            $subscriber->save();
        }
        return redirect('admin/newsletters/subscribers');
    }
    
    public function sendNewsletter($id){
        
        ini_set('max_execution_time', 2200);
        
        $newsletter = Newsletters::find($id);
        $subscribers = Subscribers::where('subscribed', '=', true)->get();
        $appMail = env('MAIL_USERNAME');
        $this->viewBag['newsletter'] = $newsletter;
        $this->viewBag['resources_path'] = public_path();
        $this->viewBag['base_url'] = url('/');
        $view = $this->viewBag;
        
        $newsletter->sent = true;
        $newsletter->update();
        
//        foreach ($subscribers as $subscriber){
//            $view['hashedMail'] = encrypt($subscriber->email);
//            Mail::queue('emails.newsletter', $view, function ($message) use ($subscriber, $view, $appMail ) {
//                $message->from($appMail, $view['newsletter']->title);
//                $message->to($subscriber->email)
//                        ->subject($view['newsletter']->title);
//            });
//        }
        
        return redirect('admin/newsletters');
    }
    
    
}

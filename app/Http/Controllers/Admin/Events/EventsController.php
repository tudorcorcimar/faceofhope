<?php

namespace App\Http\Controllers\Admin\Events;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use App\Models\EventSubscribers;

class EventsController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'eventsubscribers';
        $this->viewBag['subscribers'] = EventSubscribers::all();
        return View::make("admin.events.subscribers", $this->viewBag);
    }
}

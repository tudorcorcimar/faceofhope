<?php

namespace App\Http\Controllers\Admin\Home;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;

class HomeController extends AdminBaseController
{
    public function index() {
        return View::make("admin.home.view", $this->viewBag);
    }
}

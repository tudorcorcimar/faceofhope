<?php

namespace App\Http\Controllers\Admin\Donors;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use App\Models\User;
use Illuminate\Support\Facades\Input;
use App\Models\Donations;

class DonorsController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'donations';
        $this->viewBag['donations'] = Donations::paginate(50);
        return View::make("admin.donors.donations", $this->viewBag);
    }
    
    public function showCreatingDonationForm($userId) {
        $this->viewBag['category'] = 'conreatedonat';
        $this->viewBag['donating'] = [
            'user' => User::find($userId)
        ];
        return View::make("admin.donors.create_donation", $this->viewBag);
    }
    
    public function createDonation() {
        $donation = new Donations;
        $donation->user_id = Input::get('user_id');
        $donation->money = Input::get('money');
        $donation->comments = Input::get('comments');
        $stuff = json_encode(Input::get('stuff'));
        $donation->stuff = $stuff;
        $donation->save();
        
        return redirect('admin/donors/');
    }
    
    public function removeDonation($donationId) {
        $donation = Donations::find($donationId);
        $donation->delete();
        
        return redirect('admin/donors/');
    }
}

<?php

namespace App\Http\Controllers\Admin\App;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use App\Models\App;
use Illuminate\Support\Facades\Input;
use App\Models\Admin;
use Illuminate\Support\Facades\Validator;

class AppController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'app';
        return View::make("admin.app.view", $this->viewBag);
    }
    
    public function updateAppInfo() {
        $app = App::first();
        
        $app->name = Input::get('name');
        $app->phone = Input::get('phone');
        $app->fax = Input::get('fax');
        $app->email = Input::get('email');
        $app->address = Input::get('address');
        $app->url = Input::get('url');
        $app->description = Input::get('description');
        $app->update();
        
        return redirect('admin/app');
    }
    
    public function updateAdminPassword() {
        
        $validator = Validator::make(Input::all(), [
            'password' => 'required|min:6|confirmed'
        ]);
        
        if($this->viewBag['user']->getUser()->password == Input::get('current_password')){
            if($validator->fails()){
                return redirect("admin/app/")->withErrors($validator)->withInput();
            }else{
                $admin = Admin::where('login', $this->viewBag['user']->getUser()->login)->first();
                
                $admin->password = Input::get('password');
                $admin->update();
                return redirect("admin/app/")->with('success-message', 'Password successfully updated!');
            }
        }else{
            return redirect("admin/app/")->with('error-message', 'Wrong current password!');
        }
        
    }
}

<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\User;

class UsersController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'users';
        $this->viewBag['users'] = User::orderBy('updated_at', 'desc')->paginate(50);
        return View::make("admin.users.search", $this->viewBag);
    }
    
    public function search() {
        $this->viewBag['category'] = 'users';
        $this->viewBag['form'] = Input::all();
        $keywords = Input::get('keywords');
        $users = User::where('first_name', 'LIKE', '%' . $keywords . '%')
                     ->orWhere('last_name', 'LIKE', '%' . $keywords . '%')
                     ->orWhere('middle_name', 'LIKE', '%' . $keywords . '%')
                     ->orWhere('email', 'LIKE', '%' . $keywords . '%');
        
        $this->viewBag['users'] = $users->orderBy('updated_at', 'desc')->paginate(50);
        
        return View::make("admin.users.search", $this->viewBag);
    }
    
}

<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileTypes;
use App\Models\Donor;
use App\Models\Needy;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\Volunteer;

class CreateUsersController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'createusers';
        return View::make("admin.users.create", $this->viewBag);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  Input::all()
     * @return User
     */
    protected function createUser()
    {
        $validator = Validator::make(Input::all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user',
            'phone' => 'required|max:50',
            'types' => 'required|not_in:0',
            'password' => 'required|min:6|confirmed',
            'address' => 'required',
            'state' => 'required',
            'birth_day' => 'required',
            'city' => 'required',
            'zip' => 'required',
        ]);
        
        if($validator->fails()){
            
            Input::flash();
            return redirect("admin/users/create")->withErrors($validator);;
        }
        
        $isNeedy = false;
        $isDonor = false;
        $isVolunteer = false;
        
        $user = User::create([
            'first_name' => Input::all()['first_name'],
            'middle_name' => Input::all()['middle_name'],
            'last_name' => Input::all()['last_name'],
            'email' => Input::all()['email'],
            'active' => true,
            'password' => bcrypt(Input::all()['password'])
        ]);
        
        $userProfile = new UserProfile;
        $userProfile->user_id = $user->id;
        $userProfile->phone = Input::all()['phone'];
        $userProfile->address = Input::all()['address'];
        $userProfile->city = Input::all()['city'];
        $userProfile->state = Input::all()['state'];
        $userProfile->zip = Input::all()['zip'];
        $userProfile->birth_day = Carbon::createFromFormat('F.d.Y', Input::all()['birth_day']);
        $userProfile->emergency_contact = Input::all()['emergency_contact'];
        $userProfile->save();
        
        if(isset(Input::all()['types']) && is_array(Input::all()['types']) ){
            foreach(Input::all()['types'] as $key => $typeId){
                if( $key === 3 || $key === 4 ){
                    $isNeedy = true;
                }
                if( $key === 1 ){
                    $isDonor = true;
                }
                if( $key === 2 ){
                    $isVolunteer = true;
                }
                $userProfileTypes = new UserProfileTypes;
                $userProfileTypes->user_id = $user->id;
                $userProfileTypes->type_id = $key;
                $userProfileTypes->save();
            }
        }
        
        if($isNeedy){
            $details = json_encode(Input::all()['needy']);
            $needy = new Needy;
            $needy->user_id = $user->id;
            $needy->details = $details;
            $needy->save();
        }
        
        if($isDonor){
            $donor = new Donor;
            $donor->user_id = $user->id;
            $donor->active = true;
            $donor->save();
        }
        
        if($isVolunteer){
            $volunteer = new Volunteer;
            $volunteer->user_id = $user->id;
            $volunteer->active = true;
            $volunteer->save();
        }
        
        return redirect("admin/users/edit/$user->id");
    }
    
    
}

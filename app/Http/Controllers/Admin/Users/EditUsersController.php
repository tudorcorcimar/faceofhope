<?php

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use App\Models\App;
use Illuminate\Support\Facades\Input;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\UserProfileTypes;
use App\Models\Donor;
use App\Models\Needy;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Models\JobsApply;
use App\Models\VolunteerShedule;

class EditUsersController extends AdminBaseController
{
    public function index($userId) {
        $this->viewBag['needy'] = $this->getNeedy($userId);
        $this->viewBag['category'] = 'editusers';
        $this->viewBag['userdata'] = User::find($userId);
        return View::make("admin.users.edit", $this->viewBag);
    }
    
    public function switchUserActiveStatus($userId) {
        $user = User::find($userId);
        $user->active = !$user->active;
        $user->update();
        
        return redirect('admin/users');
    }
    
    public function removeUser($userId) {
        $user = User::find($userId);
        $userPrfile = UserProfile::where('user_id', '=', $userId);
        $userProfileTypes = UserProfileTypes::where('user_id', '=', $userId);
        $donor = Donor::where('user_id', '=', $userId);
        $needy = Needy::where('user_id', '=', $userId);
        $applies = JobsApply::where('user_id', '=', $userId);
        $shedule = VolunteerShedule::where('user_id', '=', $userId);
        
        $user->delete();
        $userPrfile->delete();
        $userProfileTypes->delete();
        $donor->delete();
        $needy->delete();
        $applies->delete();
        $shedule->delete();
        
        return redirect('admin/users');
    }
    
    /**
     * Update the user profile settings.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserSettings($userId) 
    {
        $validator = $this->validateUserSettings();
        if($validator->fails()){
            return redirect("admin/users/edit/$userId")->withErrors($validator)->withInput();
        }
        $this->updateUser($userId);
        
        return redirect("admin/users/edit/$userId")->with('success-message', 'Settings successfully updated!');
    }
    

    /**
     * Validate settings input data.
     *
     * @return Illuminate\Support\Facades\View
     */
    public function validateUserSettings() {
        return Validator::make(Input::all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'phone' => 'required|max:50',
            'address' => 'required',
            'state' => 'required',
            'birth_day' => 'required',
            'city' => 'required',
            'zip' => 'required',
        ]);
    }


    /**
     * Update user.
     *
     * @return App\Models\User
     */
    private function updateUser($userId) 
    {
        $input = Input::all();
        $user = User::find($userId);
        
        $user->first_name = $input['first_name'];
        $user->middle_name = $input['middle_name'];
        $user->last_name = $input['last_name'];
        $user->description = $input['description'];
        $user->update();
        
        $this->updateUserProfile($user->profile->id);
        
        return $user;
    }


    /**
     * Update user profile.
     *
     * @return App\Models\UserProfile
     */
    private function updateUserProfile($profileId) 
    {
        $input = Input::all();
        $userProfile = UserProfile::find($profileId);
        $userProfile->phone = $input['phone'];
        $userProfile->address = $input['address'];
        $userProfile->city = $input['city'];
        $userProfile->state = $input['state'];
        $userProfile->zip = $input['zip'];
        $userProfile->emergency_contact = $input['emergency_contact'];
        $userProfile->birth_day = Carbon::createFromFormat('F.d.Y', $input['birth_day']);
        $userProfile->update();
        
        return $userProfile;
    }


    /**
     * Update user email.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserEmail($userId) 
    {
        $validator = Validator::make(Input::all(), [
            'email' => 'required|email|max:255|unique:user',
        ]);
        if($validator->fails()){
            return redirect("admin/users/edit/$userId")->with('emailError', $validator->errors()->first());
        }
        $user = User::find($userId);
        $user->email = Input::get('email');
        $user->update();
        
        return redirect("admin/users/edit/$userId")->with('success-message', 'Email successfully updated!');
    }


    /**
     * Update user password.
     *
     * Illuminate\Support\Facades\View
     */
    public function updateUserPassword($userId) 
    {
        $validator = Validator::make(Input::all(), [
            'password' => 'required|min:6|confirmed',
        ]);
        if($validator->fails()){
            return redirect("admin/users/edit/$userId")->with('passwordError', $validator->errors()->first());
        }
        $user = User::find($userId);
        $user->password = bcrypt(Input::get('password'));
        $user->update();
        
        return redirect("admin/users/edit/$userId")->with('success-message', 'Password successfully updated!');
    }
    
    


    /**
     * Try to find needy profile and if it exists return it or return empty array.
     * 
     * @param $userId int
     * 
     * @return $data array
     */
    private function getNeedy($userId) {
        $data = [];
        $needy = Needy::where('user_id', '=', $userId)->first();
        if($needy){
            $data = [
                'id' => $needy->id,
                'active' => $needy->active,
                'details' => json_decode($needy->details, true)
            ];
        }
        return $data;
    }


    /**
     * Opdate needy data.
     * 
     * @param $userId int
     * 
     * @return Illuminate\Support\Facades\View
     */
    public function updateNeeds($userId) {
        $needy = Needy::where('user_id', '=', $userId)->first();
        
        if(!$needy){
            $needy = new Needy;
            $needy->user_id = $userId;
            $needy->details = json_encode(Input::get('needy'));
            $needy->active = true;
            $needy->save();
        }else{
            $needy->details = json_encode(Input::get('needy'));
            $needy->update();
        }
        
        $this->viewBag['needy'] = $this->getNeedy($userId);
        return redirect("admin/users/edit/$userId");
    }
    
    
}

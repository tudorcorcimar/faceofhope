<?php

namespace App\Http\Controllers\Admin\Contacts;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Contacts;
use Illuminate\Support\Facades\File;

class ContactsController extends AdminBaseController
{
    
    public function showAdministration() {
        $this->viewBag['category'] = 'contacts';
        $this->viewBag['contacts'] = Contacts::all();
        return View::make("admin.contacts.contacts", $this->viewBag);
    }
    
    public function addAdministrationInfo() {
        $contact = new Contacts;
        $contact->details = Input::get('details');
        $contact->save();
        if (Input::get('photo') && Input::get('photo') !== ''){
            $contact->photo = $this->savePhoto($contact->id, Input::get('photo'));
            $contact->update();
        }
        return redirect('admin/contacts/');
    }
    
    public function savePhoto($contactId, $base64String) {
        
        $path = public_path().'/images/contacts/';
        
        if(!File::exists($path)){
            File::makeDirectory($path, 0775, true);
        }
        
        $contactImage = $path.$contactId.'.jpg';
        
        if (File::exists($contactImage)){
            File::delete($contactImage);
        }
        
        $file = fopen($contactImage, "wb");
        $data = explode(',', $base64String);
        fwrite($file, base64_decode($data[1]));
        fclose($file); 
        
        return $contactId.'.jpg';
    }
    
    public function showEditAdministrationInfo($id) {
        $this->viewBag['category'] = 'contacts';
        $this->viewBag['contact'] = Contacts::find($id);
        return View::make("admin.contacts.edit", $this->viewBag);
    }
    
    public function editAdministrationInfo($id) {
        $contact = Contacts::find($id);
        $contact->details = Input::get('details');
        $contact->update();
        if (Input::get('photo') && Input::get('photo') !== ''){
            $this->savePhoto($contact->id, Input::get('photo'));
        }
        return redirect('admin/contacts/');
    }
    
    public function removeAdministrationInfo($id) {
        $path = public_path().'/images/contacts/';
        $contactImage = $path.$id.'.jpg';
        
        if (File::exists($contactImage)){
            File::delete($contactImage);
        }
        
        $contact = Contacts::find($id);
        $contact->delete();
        return redirect('admin/contacts/');
    }
    
}

<?php

namespace App\Http\Controllers\Admin\Jobs;

use App\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use App\Models\Jobs;
use App\Models\JobsApply;

class JobsController extends AdminBaseController
{
    public function index() {
        $this->viewBag['category'] = 'jobs';
        
        $this->viewBag['jobs'] = Jobs::orderBy('id', 'desc')->paginate(50);
        
        return View::make("admin.jobs.positions", $this->viewBag);
    }
    
    public function showAddJobForm() {
        $this->viewBag['category'] = 'addjob';
        return View::make("admin.jobs.create", $this->viewBag);
    }
    
    public function showAppliesUsers() {
        $this->viewBag['category'] = 'jobapplies';
        
        $applies = JobsApply::join('v_jobs', 'jobs_users.job_id', '=', 'v_jobs.id')
                                            ->paginate(50);
        $this->viewBag['applies'] = $applies ? $applies : [];
        
        return View::make("admin.jobs.applies", $this->viewBag);
    }
    
    public function createPosition() {
        $job = new Jobs;
        $job->title = Input::get('title');
        $job->details = Input::get('details');
        $job->save();
        
        return redirect('/admin/jobs');
    }
    
    public function showEditPosition($id) {
        $this->viewBag['job'] = Jobs::find($id);
        $this->viewBag['category'] = 'jobedit';
        return View::make("admin.jobs.edit", $this->viewBag);
    }
    
    public function editPosition($id) {
        $job = Jobs::find($id);
        $job->title = Input::get('title');
        $job->details = Input::get('details');
        $job->update();
        
        return redirect('/admin/jobs');
    }
    
    public function removePosition($jobId) {
        $job = Jobs::find($jobId);
        $job->delete();
        $jobApply = JobsApply::where('job_id', '=', $jobId);
        $jobApply->delete();
        return redirect('/admin/jobs');
    }
    
}

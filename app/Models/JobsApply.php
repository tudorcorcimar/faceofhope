<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobsApply extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs_users';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}

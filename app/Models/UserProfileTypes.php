<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserProfileTypes extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_profile_types';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}

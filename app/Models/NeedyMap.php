<?php

namespace App\Models;

class NeedyMap
{
    
    /**
     * The huge list of questions that must be used for a needy form, I decide to
     * save it in json string and map it by this model to escape from this
     * unique unpaid project =)
     * 
     * Example of json response:
     *  var example = [
     *       'title' => string,
     *       'note' => string,
     *       'clauses' => array[
     *           [ 
     *             'type' => string{ DOM Element Input Type / example: checkbox, radio, text ... }, 
     *             'name' => string{ DOM Element inner text }
     *           ]
     *       ],
             'extended' => array[{ SELF "repeted self structure for aditional subclause" }]
     *  ]
     * 
     * // If it will be extended and paid it can ease be migrated via this map =)
     * // to database or moved to a config file  
     * 
     * @var array
     */
    protected $needyMap = [
        [
            'id' => 'q_1',
            'title' => 'Gender',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Male' ],
                [ 'type' => 'radio', 'name' => 'Female' ],
                [ 'type' => 'radio', 'name' => 'Transgendered' ]
            ]
        ],
        [
            'id' => 'q_2',
            'title' => 'Ethnicity',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Hispanic / Latino' ],
                [ 'type' => 'radio', 'name' => 'Non-Hispanic / Non-Latino' ],
                [ 'type' => 'text', 'name' => 'Other' ]
            ]
        ],
        [
            'id' => 'q_3',
            'title' => 'Race',
            'note' => 'Please check all that apply',
            'clauses' => [
                [ 'type' => 'checkbox', 'name' => 'American Indian / Alaskan Native' ],
                [ 'type' => 'checkbox', 'name' => 'Asian' ],
                [ 'type' => 'checkbox', 'name' => 'Black / African American' ],
                [ 'type' => 'checkbox', 'name' => 'Native Hawaiian / Other Pacific Islander' ],
                [ 'type' => 'checkbox', 'name' => 'White' ],
                [ 'type' => 'checkbox', 'name' => 'Don’t Know' ],
            ]
        ],
        [
            'id' => 'q_4',
            'title' => 'How long have you lived in Sarasota / Charlotte County?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => '1 week or less' ],
                [ 'type' => 'radio', 'name' => '1 week to 1 month' ],
                [ 'type' => 'radio', 'name' => '1 to 3 months' ],
                [ 'type' => 'radio', 'name' => '3 months to 1 year' ],
                [ 'type' => 'radio', 'name' => '1 year or longer' ],
            ]
        ],
        [
            'id' => 'q_5',
            'title' => 'What is your housing status?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Homeless' ],
                [ 'type' => 'radio', 'name' => 'At imminent risk of losing housing' ],
                [ 'type' => 'radio', 'name' => 'Fleeing domestic violence' ],
                [ 'type' => 'radio', 'name' => 'Stably housed' ],
            ]
        ],
        [
            'id' => 'q_6',
            'title' => 'Where are you now living?',
            'note' => '*** If 1 of the 4 shaded options, check where you stayed prior to incarceration / treatment.',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Place not meant for habitation (i.e. car, street, camp)' ],
                [ 'type' => 'radio', 'name' => 'Emergency Shelter, include motel voucher' ],
                [ 'type' => 'radio', 'name' => 'Transitional Housing for Homeless' ],
                [ 'type' => 'radio', 'name' => 'Permanent Housing for Homeless' ],
                [ 'type' => 'radio', 'name' => 'Stay with family member' ],
                [ 'type' => 'radio', 'name' => 'Stay with friend' ],
                [ 'type' => 'radio', 'name' => 'Rented room, apartment, house' ],
                [ 'type' => 'radio', 'name' => 'Owned apartment or house' ],
                [ 'type' => 'radio', 'name' => 'Hotel / Motel paid for by self' ],
                [ 'type' => 'radio', 'name' => 'Foster care home' ],
                [ 'type' => 'radio', 'name' => 'Substance abuse treatment facility***' ],
                [ 'type' => 'radio', 'name' => 'Hospital***' ],
                [ 'type' => 'radio', 'name' => 'Psychiatric facility***' ],
                [ 'type' => 'radio', 'name' => 'Jail, prison, detention facility***' ],
                [ 'type' => 'text', 'name' => 'Other' ]
            ]
        ],
        [
            'id' => 'q_7',
            'title' => 'Will you be forced to leave the place you staying within the week or this month?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No (SKIP TO QUESTION 12)' ],
            ]
        ],
                [
                    'id' => 'q_7_1',
                    'title' => 'Moving?',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'radio', 'name' => 'one day, or less' ],
                        [ 'type' => 'radio', 'name' => '2 days-1 week' ],
                        [ 'type' => 'radio', 'name' => 'more than one week but less than one month' ],
                        [ 'type' => 'radio', 'name' => '1 to 3 months' ],
                        [ 'type' => 'radio', 'name' => 'more than 3 months but less than 1 year or longer' ]
                    ],
                ],
                [
                    'id' => 'q_7_2',
                    'title' => 'Will you have a place to stay OR money that you will use to get a place to stay once you leave?',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'radio', 'name' => 'Yes' ],
                        [ 'type' => 'radio', 'name' => 'No' ],
                    ],
                ],
                [
                    'id' => 'q_7_3',
                    'title' => 'Zip code at last stable address (90 days plus)',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'text', 'name' => '' ],
                    ],
                ],
        [
            'id' => 'q_8',
            'title' => 'How many separate times in the past 3 years have you been homeless?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'None (Skip to question 15)' ],
                [ 'type' => 'radio', 'name' => '1 time' ],
                [ 'type' => 'radio', 'name' => '2 – 3 times' ],
                [ 'type' => 'radio', 'name' => '4 or more times' ],
            ]
        ],
        [
            'id' => 'q_9',
            'title' => 'How long have you been homeless during this current episode?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => '1 week or less' ],
                [ 'type' => 'radio', 'name' => '1 week to 1 month' ],
                [ 'type' => 'radio', 'name' => '1 to 3 month' ],
                [ 'type' => 'radio', 'name' => '3 months to 1 year' ],
                [ 'type' => 'radio', 'name' => '1 year or longer' ],
            ]
        ],
        [
            'id' => 'q_10',
            'title' => 'What caused you to become homeless?',
            'note' => 'Please check all that apply',
            'clauses' => [
                [ 'type' => 'checkbox', 'name' => 'Employment / Financial reasons' ],
                [ 'type' => 'checkbox', 'name' => 'Housing issues / Forced to relocate from home' ],
                [ 'type' => 'checkbox', 'name' => 'Medical / Disability problems' ],
                [ 'type' => 'checkbox', 'name' => 'Family problems' ],
                [ 'type' => 'checkbox', 'name' => 'Natural / Other Disaster' ],
                [ 'type' => 'checkbox', 'name' => 'Recent immigration' ],
                [ 'type' => 'checkbox', 'name' => 'Domestic Violence' ],
                [ 'type' => 'text', 'name' => 'Other' ],
            ]
        ],
        [
            'id' => 'q_11',
            'title' => 'Do you have a medical or disabling condition?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No' ],
            ],
        ],
                [
                    'id' => 'q_11_1',
                    'title' => 'Does any household member?',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'radio', 'name' => 'Yes' ],
                        [ 'type' => 'radio', 'name' => 'No' ],
                    ],
                ],
                [
                    'id' => 'q_11_2',
                    'title' => 'If yes, what type of medical or disabling condition do you have?',
                    'note' => 'Please check all that apply',
                    'clauses' => [
                        [ 'type' => 'checkbox', 'name' => 'Physical' ],
                        [ 'type' => 'checkbox', 'name' => 'Developmental' ],
                        [ 'type' => 'checkbox', 'name' => 'Mental Health' ],
                        [ 'type' => 'checkbox', 'name' => 'Diabetes' ],
                        [ 'type' => 'checkbox', 'name' => 'Drug or Alcohol Addiction' ],
                        [ 'type' => 'checkbox', 'name' => 'HIV / AIDS' ],
                        [ 'type' => 'text', 'name' => 'Other' ],
                    ],
                ],
        [
            'id' => 'q_12',
            'title' => 'Were you ever in foster care?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No' ],
            ]
        ],
        [
            'id' => 'q_13',
            'title' => 'Current Marital Status',
            'note' => 'Choose one',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Married' ],
                [ 'type' => 'radio', 'name' => 'Domestic Partner' ],
                [ 'type' => 'radio', 'name' => 'Divorced' ],
                [ 'type' => 'radio', 'name' => 'Separated' ],
                [ 'type' => 'radio', 'name' => 'Widowed' ],
                [ 'type' => 'radio', 'name' => 'Single' ],
                [ 'type' => 'radio', 'name' => 'Common Law' ],
            ]
        ],
        [
            'id' => 'q_14',
            'title' => 'Have you been a victim of domestic violence?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No' ],
            ],
        ],
                [
                    'id' => 'q_14_1',
                    'title' => 'If Yes, when did it occur',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'radio', 'name' => '1 week or less' ],
                        [ 'type' => 'radio', 'name' => '1 week to 1 month' ],
                        [ 'type' => 'radio', 'name' => '1 to 3 month' ],
                        [ 'type' => 'radio', 'name' => '3 months to 1 year' ],
                        [ 'type' => 'radio', 'name' => '1 year or longer' ],
                    ],
                ],
                [
                    'id' => 'q_14_2',
                    'title' => 'Are you currently fleeing',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'radio', 'name' => 'Yes' ],
                        [ 'type' => 'radio', 'name' => 'No' ],
                    ],
                ],
        
        [
            'id' => 'q_15',
            'title' => 'EDUCATION: What is the highest level you completed?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'No Schooling Completed' ],
                [ 'type' => 'radio', 'name' => 'Nursery to 4th grade' ],
                [ 'type' => 'radio', 'name' => '5th grade or 6th grade' ],
                [ 'type' => 'radio', 'name' => '7th grade or 8th grade' ],
                [ 'type' => 'radio', 'name' => '9th Grade' ],
                [ 'type' => 'radio', 'name' => '10th grade' ],
                [ 'type' => 'radio', 'name' => '11th grade' ],
                [ 'type' => 'radio', 'name' => '12 grade, No Diploma' ],
                [ 'type' => 'radio', 'name' => 'High School Diploma' ],
                [ 'type' => 'radio', 'name' => 'GED' ],
                [ 'type' => 'radio', 'name' => 'Post-Secondary /2 yr. degree' ],
                [ 'type' => 'radio', 'name' => 'College/4 yr. degree' ],
                [ 'type' => 'radio', 'name' => 'Master or above' ],
            ]
        ],
        [
            'id' => 'q_16',
            'title' => 'What type of transportation do you currently rely on?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Bicycle' ],
                [ 'type' => 'radio', 'name' => 'Own Car' ],
                [ 'type' => 'radio', 'name' => 'Rent Car' ],
                [ 'type' => 'radio', 'name' => 'Family/Friends' ],
                [ 'type' => 'radio', 'name' => 'Taxi' ],
                [ 'type' => 'radio', 'name' => 'Bus' ],
                [ 'type' => 'radio', 'name' => 'Walk/None' ],
            ]
        ],
        [
            'id' => 'q_17',
            'title' => 'Do you receive any of the following forms of income?',
            'note' => 'Please include all household income and check all that apply',
            'clauses' => [
                [ 'type' => 'checkbox', 'name' => 'Earned Income' ],
                [ 'type' => 'checkbox', 'name' => 'Unemployment' ],
                [ 'type' => 'checkbox', 'name' => 'SSI' ],
                [ 'type' => 'checkbox', 'name' => 'SSDI' ],
                [ 'type' => 'checkbox', 'name' => 'Veterans Disability' ],
                [ 'type' => 'checkbox', 'name' => 'Private Disability' ],
                [ 'type' => 'checkbox', 'name' => 'Worker’s Comp.' ],
                [ 'type' => 'checkbox', 'name' => 'TANF' ],
                [ 'type' => 'checkbox', 'name' => 'General Assistance' ],
                [ 'type' => 'checkbox', 'name' => 'SSA Retirement' ],
                [ 'type' => 'checkbox', 'name' => 'Veteran’s Pension' ],
                [ 'type' => 'checkbox', 'name' => 'Job Pension' ],
                [ 'type' => 'checkbox', 'name' => 'Child Support' ],
                [ 'type' => 'checkbox', 'name' => 'Alimony' ],
                [ 'type' => 'checkbox', 'name' => 'Other Source' ],
                [ 'type' => 'checkbox', 'name' => 'No Financial Resources' ],
            ]
        ],
        [
            'id' => 'q_18',
            'title' => 'Total income from above $',
            'note' => '',
            'clauses' => [
                [ 'type' => 'text', 'name' => '' ],
            ],
        ],
                [
                    'id' => 'q_18_1',
                    'title' => 'Amount of non-cash income $',
                    'note' => 'food stamps, etc',
                    'clauses' => [
                        [ 'type' => 'text', 'name' => '' ],
                    ],
                ],
        [
            'id' => 'q_19',
            'title' => 'Do you have any children under the age of 18 that aren’t with you?',
            'note' => '',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No' ],
            ]
        ],
        [
            'id' => 'q_20',
            'title' => 'Do you have any family members living with you?',
            'note' => 'If yes, please complete the Family Survey below',
            'clauses' => [
                [ 'type' => 'radio', 'name' => 'Yes' ],
                [ 'type' => 'radio', 'name' => 'No' ],
            ],
            
            
            
            'multiple' => [
                        [
                            'id' => 'q_1',
                            'title' => 'Family member.',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'text', 'name' => 'First Name' ],
                                [ 'type' => 'text', 'name' => 'Last Name' ],
                                [ 'type' => 'text', 'name' => 'Date of Birth Or Age' ],
                            ],
                        ],
                        [
                            'id' => 'q_2',
                            'title' => 'Gender',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'radio', 'name' => 'Male' ],
                                [ 'type' => 'radio', 'name' => 'Female' ],
                                [ 'type' => 'radio', 'name' => 'Transgender' ],
                            ],
                        ],
                        [
                            'id' => 'q_3',
                            'title' => 'Relationship to you',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'radio', 'name' => 'Husband' ],
                                [ 'type' => 'radio', 'name' => 'Wife' ],
                                [ 'type' => 'radio', 'name' => 'Domestic Partner' ],
                                [ 'type' => 'radio', 'name' => 'Son' ],
                                [ 'type' => 'radio', 'name' => 'Daughter' ],
                                [ 'type' => 'radio', 'name' => 'Mother' ],
                                [ 'type' => 'radio', 'name' => 'Father' ],
                                [ 'type' => 'radio', 'name' => 'Grandmother' ],
                                [ 'type' => 'radio', 'name' => 'Grandfather' ],
                                [ 'type' => 'radio', 'name' => 'Grandchild' ],
                                [ 'type' => 'radio', 'name' => 'Brother' ],
                                [ 'type' => 'radio', 'name' => 'Sister' ],
                                [ 'type' => 'radio', 'name' => 'Aunt' ],
                                [ 'type' => 'radio', 'name' => 'Uncle' ],
                                [ 'type' => 'radio', 'name' => 'Niece / Nephew' ],
                                [ 'type' => 'text', 'name' => 'Other (Specify)' ],
                            ],
                        ],
                        [
                            'id' => 'q_4',
                            'title' => 'Ethnicity',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'radio', 'name' => 'Hispanic / Latino' ],
                                [ 'type' => 'radio', 'name' => 'Non-Hispanic / Non-Latino' ],
                            ],
                        ],
                        [
                            'id' => 'q_5',
                            'title' => 'Race',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'radio', 'name' => 'American Indian or Alaska Native' ],
                                [ 'type' => 'radio', 'name' => 'Asian' ],
                                [ 'type' => 'radio', 'name' => 'Black or African American' ],
                                [ 'type' => 'radio', 'name' => 'Native Hawaiian or Other Pacific Islander' ],
                                [ 'type' => 'radio', 'name' => 'White' ],
                            ],
                        ],
                        [
                            'id' => 'q_6',
                            'title' => 'Disabling condition',
                            'note' => '',
                            'clauses' => [
                                [ 'type' => 'radio', 'name' => 'Yes' ],
                                [ 'type' => 'radio', 'name' => 'No' ],
                            ],
                        ],
                    ]
            
            
            
            
            
            
            
        ],
        
        
        [
            'id' => 'q_21',
            'title' => 'Services that you or your family need right now',
            'note' => 'Check all that apply',
        ],
                [
                    'id' => 'q_21_1',
                    'title' => 'Child donation',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'checkbox', 'name' => 'Food' ],
                        [ 'type' => 'checkbox', 'name' => 'Hygiene products' ],
                        [ 'type' => 'checkbox', 'name' => 'Child and Infant Clothing' ],
                        [ 'type' => 'checkbox', 'name' => 'Diapers' ],
                        [ 'type' => 'checkbox', 'name' => 'Books and Toys' ],
                        [ 'type' => 'checkbox', 'name' => 'Baby Food' ],
                        [ 'type' => 'checkbox', 'name' => 'Shoes' ],
                        [ 'type' => 'checkbox', 'name' => 'Milk / Formula' ],
                        [ 'type' => 'checkbox', 'name' => 'Bicycle' ],
                        [ 'type' => 'checkbox', 'name' => 'School Supplies' ],
                        [ 'type' => 'checkbox', 'name' => 'Snacks' ],
                    ],
                ],
                [
                    'id' => 'q_21_2',
                    'title' => 'Adult donation',
                    'note' => '',
                    'clauses' => [
                        [ 'type' => 'checkbox', 'name' => 'Case' ],
                        [ 'type' => 'checkbox', 'name' => 'Case Management' ],
                        [ 'type' => 'checkbox', 'name' => 'Food' ],
                        [ 'type' => 'checkbox', 'name' => 'Housing Search/Placement' ],
                        [ 'type' => 'checkbox', 'name' => 'Temporary Housing' ],
                        [ 'type' => 'checkbox', 'name' => 'Adult Diapers' ],
                        [ 'type' => 'checkbox', 'name' => 'Employment' ],
                        [ 'type' => 'checkbox', 'name' => 'Hygiene products' ],
                        [ 'type' => 'checkbox', 'name' => 'Clothing' ],
                        [ 'type' => 'checkbox', 'name' => 'Day Care/Child Care' ],
                        [ 'type' => 'checkbox', 'name' => 'Transportation/Gas' ],
                        [ 'type' => 'checkbox', 'name' => 'Bus Passes – single / monthly' ],
                        [ 'type' => 'checkbox', 'name' => 'Community Hours' ],
                        [ 'type' => 'checkbox', 'name' => 'NO SERVICES NEEDED' ],
                        [ 'type' => 'checkbox', 'name' => 'Other' ],
                    ],
                ],
        
    ];
    
    public function getNeedyMap() {
        return $this->needyMap;
    }
    
}

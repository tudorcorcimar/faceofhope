<?php
namespace App\Models\Runtime;

use App\Models\Admin;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;

/**
 * Admin User Authentication
 *
 * @author Tudor Corcimar
 * @version 05/07/2016
 * @package App/Models/Runtime/Auth
 * @copyright (c) 2016, IronBoard
 */
class AdminAuth {

    private $isAuth;
    private $user;
    
    //singleton instance
    private static $instance;
    
    public function getIsAuth() {
        $isRememberedId = Cookie::get('adminAuthId');
        if($isRememberedId && !$this->isAuth){
            $this->user = Admin::where('id', $isRememberedId)->first();
            if(!empty($this->user)){
                return $this->login($this->user->login, $this->user->password);
            }
            return false;
        }else{
            return $this->isAuth;
        }
    }
    
    public function getUser() {
        return $this->user;
    }
    
    public function getActive() {
        return $this->active;
    }

    /**
     * Singleton
     * The constructor is private
     */
    private function __construct(){
        if(Session::has('admin.user.id')){
            $this->user = Admin::where('id', Session::get('admin.user.id'))->first();
            if(!empty($this->user)){
                $this->isAuth = true;
            } else {
                $this->isAuth = false;
            }
        }
    }

    /**
     * Singleton
     * Instance generator
     */
    public static function getInstance(){
        if(self::$instance instanceof self){
            return self::$instance;
        } else {
            self::$instance = new self();
            return self::$instance;
        }
    }

    /**
     * Checks username/password validity loads properties and stores auth data in session
     *
     * @param String $username
     * @param String $password
     * @return boolean
     */
    public function login($username = "", $password = "", $rememberMe = false){
        
        $user = Admin::where('login', $username)->first();
        
        if($user){
            $this->user = $user;
            if($this->user->password == $password){
                $this->isAuth = true;
                
                Session::put('admin.user.id', $this->user->id);

                if($rememberMe){
                    Cookie::queue('adminAuthId', $this->user->id, 1440);
                }
                
                return true;
            } else{
                $this->isAuth = false;
                return false;
            }

        } else {
            $this->isAuth = false;
            return false;
        }
    }

    /**
     * Log out
     *
     * @return void
     */
    public function logout(){
        $this->isAuth = false;
        $this->user = null;
        Cookie::queue(Cookie::forget('adminAuthId'));
        Session::forget('admin.user.id');
    }
}
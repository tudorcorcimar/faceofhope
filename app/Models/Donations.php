<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donations extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'donations';
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventSubscribers extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_subscribers';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}

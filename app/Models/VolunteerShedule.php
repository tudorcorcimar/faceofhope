<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VolunteerShedule extends Model
{
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'v_shedule';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
}

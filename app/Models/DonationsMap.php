<?php

namespace App\Models;

class DonationsMap
{
    private $donationsMap = [
        'Food',
        'Baby Food',
        'Hygiene products',
        'Child and Infant Clothing',
        'Diapers',
        'Adult Diapers',
        'Books and Toys',
        'Shoes',
        'Milk / Formula',
        'Bicycle',
        'School Supplies',
        'Snacks',
        'Case',
        'Case Management',
        'Housing Search/Placement',
        'Temporary Housing',
        'Employment',
        'Clothing',
        'Day Care/Child Care',
        'Transportation/Gas',
        'Bus Passes – single / monthly',
        'Community Hours',
        'Other',
    ];
    
    public function getDonationsMap() {
        return $this->donationsMap;
    }
    
}

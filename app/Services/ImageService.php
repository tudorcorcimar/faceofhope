<?php

namespace App\Services;

use Illuminate\Support\Facades\File;

class ImageService
{
    const MAX_IMAGE_COUNT = 1;
    
    protected $path;
    
    public function __construct(){
        $this->path = public_path().'/images/users/';
        if(!File::exists($this->path)){
            File::makeDirectory($this->path, 0777, true);
        }
    }
    
    public function replacePhoto($imagesInBase64, $userId){
        
        $path = $this->path.'/'.$userId;
        File::deleteDirectory($path);
        if(!File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }
        $this->savePhoto($imagesInBase64, $path);
    }
    
    private function savePhoto($base64String, $path) {
        $filePath = $path.'/photo.jpg';
        $file = fopen($filePath, "wb");
        $data = explode(',', $base64String);
        fwrite($file, base64_decode($data[1]));
        fclose($file); 
        return $filePath;
    }
    
    
    private function convertJpgToBase64($imagePath) {
        if(File::exists($imagePath)) {
            $type = pathinfo($imagePath, PATHINFO_EXTENSION);
            $data = file_get_contents($imagePath);
            $base64String = 'data:image/' . $type . ';base64,' . base64_encode($data);
        }
        return $base64String;
    }
    
    public function getPhoto($userId){
        $path = $this->path.'/'.$userId;
        $images = [];
        if(count(File::files($path)) > 0 ){
            $images = File::allFiles($path);
            return $this->convertJpgToBase64($path.'/'.$images[0]->getFilename());
        }
        return null;
    }
    
}
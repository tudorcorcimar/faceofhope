<?php

namespace App\Services;

use App\Models\User;
use App\Models\UserProfileTypes;
use App\Models\JobsApply;
use App\Models\Newsletters;
use App\Models\Volunteer;
use App\Models\Donations;
use App\Models\Donor;
use App\Models\Needy;
use App\Services\ImageService;

class Utils
{
    public function getQuestionId($string) {
        $code = str_replace('q_', '', $string);
        $qNumber = str_replace('_', '.', $code);
        
        return $qNumber;
    }
    
    public function getUserById($userId) {
        return User::find($userId);
    }
    
    public function jsonDecode($json) {
        return json_decode($json, true);
    }
    
    public function isVolunteer($userId) {
        $isVolunteer = UserProfileTypes::where('user_id', '=', $userId)
                       ->where('type_id', '=', 2)->get()->first();
        
        return isset($isVolunteer->id);
    }
    
    public function isDonor($userId) {
        $isVolunteer = UserProfileTypes::where('user_id', '=', $userId)
                       ->where('type_id', '=', 1)->get()->first();
        
        return isset($isVolunteer->id);
    }
    
    public function  isApplied($userId, $jobId){
        $jobsApply = JobsApply::where('user_id', '=', $userId)
                       ->where('job_id', '=', $jobId)->get()->first();
        
        return isset($jobsApply->id);
    }
    
    public function getLastNewsletter() {
        $newsletter = Newsletters::orderBy('created_at', 'desc')->first();
        $data = null;
        if($newsletter){
            $data = [
                'id' => $newsletter->id,
                'title' => $newsletter->title,
                'details' => strip_tags($newsletter->details)
            ];
        }
        return $data;
    }
    
    public function getStatistics() {
        $statistics = [
            'volunteers' => Volunteer::count(),
            'donors' => Donor::count(),
            'needy' => Needy::count(),
            'donated' => Donations::all()->sum('money')
        ];
        return $statistics;
    }
    
    public function getRandomUsersByType($type, $limit = 12) {
        
        return UserProfileTypes::where('type_id', '=', $type)
                               ->inRandomOrder()
                               ->limit($limit)
                               ->get();
    }
    
    public function userPhoto($userId) {
        $imageService = new ImageService;
        return $imageService->getPhoto($userId);
    }
}

<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\UserTypes;
use App\Models\App;
use App\Models\States;
use App\Models\Cities;
use App\Models\NeedyMap;
use App\Services\Utils;
use App\Models\DonationsMap;

class AdminViewBagService
{
    
    public $app;
    
    public $userTypes;
    
    public $states;
    
    public $needyMap;
    
    public $utils;
    
    public $donationsMap;
    
    
    public function __construct() 
    {
        $this->setAppInfo();
        $this->setUserTypes();
        $this->setStates();
        $this->setNeedyMap();
        $this->setUtils();
        $this->setDonationsMap();
    }
    
    
    function setAppInfo()
    {
        $this->app = App::first();
    }
    
    
    function setUserTypes()
    {
        $this->userTypes = UserTypes::all();
    }
    
    function getCitiesByState($state = 'FL') {
        
        return Cities::where('state_code', 'like', '%'.$state.'%')
                ->select(DB::RAW('DISTINCT(city), state_code'))
                ->distinct()->get();
    }
    
    function setStates() {
        $this->states = States::all();
    }
    
    function setNeedyMap(){
        $needy = new NeedyMap();
        $this->needyMap = $needy->getNeedyMap();
    }
    
    function setDonationsMap(){
        $needy = new DonationsMap();
        $this->donationsMap = $needy->getDonationsMap();
    }
    
    function setUtils(){
        $this->utils = new Utils;
    }
    
}

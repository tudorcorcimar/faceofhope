<?php

namespace App\Services;

use Illuminate\Support\Facades\DB;
use App\Models\UserTypes;
use App\Models\App;
use App\Models\States;
use App\Models\Cities;
use App\Models\NeedyMap;
use App\Services\Utils;

class ViewBagService
{
    
    public $app;
    
    public $isAuth;
    
    public $user;
    
    public $userTypes;
    
    public $form;
    
    public $breadcrumbs;
    
    public $category;
    
    public $states;
    
    public $needyMap;
    
    public $utils;
    
    
    public function __construct() 
    {
        $this->setAppInfo();
        $this->setUserTypes();
        $this->setStates();
        $this->setNeedyMap();
        $this->setUtils();
    }
    
    
    function setAppInfo()
    {
        $this->app = App::first();
    }

    
    function setIsAuth($isAuth) 
    {
        $this->isAuth = $isAuth;
    }
    
    
    function setUser($user) 
    {
        $this->user = $user;
    }

    
    function setForm($form) 
    {
        $this->form = $form;
    }
    
    
    function setBreadcrumbs($router) 
    {
        $breadcrumbs = [];
        $routes = explode('/', $router->uri());
        foreach($routes as $route){
            if( isset($route) 
                && $route !== '' 
                && strpos($route, 'token') === false )
            {
                array_push($breadcrumbs, $route);
            }
        }
        $this->setCategoryByRoutePrefix($breadcrumbs);
        
        $this->breadcrumbs = $breadcrumbs;
    }
    
    
    function setUserTypes()
    {
        $this->userTypes = UserTypes::all();
    }
    
    
    function setCategoryByRoutePrefix($breadcrumbs)
    {
        $category = null;
        $categoryExist = isset($breadcrumbs[0]);
        if($categoryExist){
            $category = $breadcrumbs[0];
        }
        
        $this->category = $category;
    }
    
    function getCitiesByState($state = 'FL') {
        
        return Cities::where('state_code', 'like', '%'.$state.'%')
                ->select(DB::RAW('DISTINCT(city), state_code'))
                ->distinct()->get();
    }
    
    function setStates() {
        $this->states = States::all();
    }
    
    function setNeedyMap(){
        $needy = new NeedyMap();
        $this->needyMap = $needy->getNeedyMap();
    }
    
    function setUtils(){
        $this->utils = new Utils;
    }
    
}
